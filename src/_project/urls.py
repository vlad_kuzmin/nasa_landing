from django.contrib import admin
from django.urls import path
from _project.settings import DEBUG, MEDIA_URL, MEDIA_ROOT
from django.conf.urls.static import static


urlpatterns = [
    # admin urls
    path("admin/", admin.site.urls),
    # project urls
]

if DEBUG:
    urlpatterns += static(MEDIA_URL, document_root=MEDIA_ROOT)
